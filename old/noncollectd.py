#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Fetch values from an X aircontrol trox technink, Zone master HVAC system

import requests
from urllib import parse # Parse response

''' Config '''
import xaircontrol_config
plugin_name = 'xaircontrol'
basicdata="command=get&sess_id=0&access_level=0"
postdata={
    'command': 'get',
    'sess_id': 0,
    'access_level': 0,
}
if xaircontrol_config.verbose:
    import pprint

types = {
    'co2': 'ppm',
    'temperature': '°C',
    'setpointpercent': '%',
    'setpointcubic': 'm3/h',
    'flowpercent': '%',
    'airmode': '0=auto,',
    'operationmode': '0=normal,',
}

def read_func():
    response = fetchdata(build_postdata ())
    if xaircontrol_config.verbose:
        print(response)

    ''' Convert raw response to dict '''
    params = dict(parse.parse_qsl(response))
    # {'command': 'get', 'sess_id': '0', 'access_level': '0', '2565': '398', '2562': '17.4', '800': '0', '2571': '0', '2526': '0.0', '2527': '120'}

    ''' Update sensor table with response values '''
    for sid in xaircontrol_config.sensors:
        if sid in params.keys():
            xaircontrol_config.sensors[sid]['value'] = params[sid]

    if xaircontrol_config.verbose:
        pprint.pprint (xaircontrol_config.sensors)

    # {
    #  '2526': {'description': 'Setpoint Supply1',
    #           'room': 'meetingroom01',
    #           'type': 'setpointpercent',
    #           'value': '0.0'},
    #  '2527': {'description': 'Setpoint Supply1',
    #           'room': 'meetingroom01',
    #           'type': 'setpointcubic',
    #           'value': '120'},
    #  '2562': {'room': 'meetingroom01', 'type': 'temperature', 'value': '17.3'},
    #  '2565': {'room': 'meetingroom01', 'type': 'co2', 'value': '397'},
    #  '2571': {'room': 'meetingroom01', 'type': 'airmode', 'value': '0'},
    #  '800': {'room': 'meetingroom01', 'type': 'operationmode', 'value': '0'}
    # }

''' Gather IDs from sensors '''
def build_postdata():
    d = postdata
    for sid in xaircontrol_config.sensors.keys():
            d[str(sid)] = ''
    return d

''' Web request '''
def fetchdata(data):
    r = requests.post(url = xaircontrol_config.apiurl, data = data, timeout=10)
    if xaircontrol_config.verbose:
        print("url %s" % r.url)
        print ("post data %s" % data)
    return r.text

read_func()