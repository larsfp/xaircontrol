#!/usr/bin/env python3
# -*- coding: utf-8 -*-
 
#collectd larstest

# config:
# LoadPlugin python
# <Plugin python>
#     ModulePath "/opt/collectd_plugins"
#     Import "larsfptest"
#     <Module larsfptest>
#         Path "/sys/class/thermal/thermal_zone0/temp"
#     </Module>
# </Plugin>

# $ sudo cp test.py /usr/lib/collectd/larsfptest.py

import collectd
import random

plugin_name = "larsfptest"

def config_func(config):
    collectd.info('%s plugin: configed' % plugin_name)

def read_func():
    testvalue1 = random.random()
    testvalue2 = random.random()*10

    # Dispatch value to collectd
    val = collectd.Values(type='gauge')
    val.plugin = plugin_name + '1'
    val.dispatch(values=[testvalue1])
    collectd.info('%s plugin: sent testvalue1 %s' % (plugin_name, testvalue1))

    val = collectd.Values(type='gauge')
    val.plugin = plugin_name + '2'
    val.dispatch(values=[testvalue2])
    collectd.info('%s plugin: sent testvalue2 %s' % (plugin_name, testvalue2))


collectd.register_config(config_func)
collectd.register_read(read_func)