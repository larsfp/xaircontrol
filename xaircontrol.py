#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Fetch values from an X aircontrol trox technink, Zone master HVAC system

import sys
from collections import OrderedDict
import requests
#from urllib import parse # Parse response

''' Config '''
import xaircontrol_config
plugin_name = 'xaircontrol'
plugin_version = '1'

postdata=OrderedDict([
     ('command', 'get'),
     ('sess_id', 0),
     ('access_level', 0),
])
if xaircontrol_config.verbose:
    import pprint

# types = {
#     'co2': 'ppm',
#     'temperature': '°C',
#     'setpointpercent': '%',
#     'setpointcubic': 'm3/h',
#     'flowpercent': '%',
#     'airmode': '0=auto,',
#     'operationmode': '0=normal,',
# }

''' Collectd config '''
def config_func(config):
    if xaircontrol_config.verbose: collectd.info('%s plugin: config' % plugin_name)

''' Collectd init '''
def init():
    """
    This method has been registered as the init callback; this gives the plugin a way to do startup
    actions.  We'll just log a message.
    :return: None
    """
    if xaircontrol_config.verbose: collectd.info('%s plugin: init' % plugin_name)

''' Collectd read '''
def read_func():
    response = fetchdata(build_postdata ())
    if xaircontrol_config.verbose:
        print(response)

    ''' Convert raw response to dict '''
    params = dict(parse_qsl(response))
    # {'command': 'get', 'sess_id': '0', 'access_level': '0', '2565': '398', '2562': '17.4', '800': '0', '2571': '0', '2526': '0.0', '2527': '120'}

    ''' Dispatch sensors to collectd '''    
    for sid in xaircontrol_config.sensors:
        if xaircontrol_config.verbose:
            collectd.info('%s plugin: *sensor  %s' % (plugin_name, sid))
        val = collectd.Values(type='gauge', plugin = plugin_name)
        try:
            val.plugin_instance = xaircontrol_config.sensors[sid]['room'] \
                + '-' + \
                xaircontrol_config.sensors[sid]['description'].replace(' ', '_')
        except KeyError:
            collectd.warn('%s plugin: missing room/description for %s' % (plugin_name, sid))
            continue

        if sid in params:
            val.values = [params[sid].decode()]
            if xaircontrol_config.verbose: 
                collectd.info(
                    '%s plugin: sending <%s> value <%s>' \
                        % (
                            plugin_name,
                            xaircontrol_config.sensors[sid]['room'] \
                            + '-' + \
                            xaircontrol_config.sensors[sid]['description'].replace(' ', '_'), params[sid]
                        ))
            val.dispatch()
            #collectd.info('%s plugin: dispatched %s' % (plugin_name, val.plugin_instance))
        else:
            collectd.info('%s plugin: missing value for %s' % (plugin_name, sid))

''' Gather IDs from sensors '''
def build_postdata():
    global postdata
    for sid in xaircontrol_config.sensors.keys():
        postdata[str(sid)] = ''
    return postdata

''' Web request '''
def fetchdata(data):
    r = None
    try:
        r = requests.post(
            url = xaircontrol_config.apiurl,
            data = data,
            timeout=5)
    except requests.ConnectTimeout:
        collectd.error('%s plugin: Error connecting to API, exiting.' % plugin_name)
        sys.exit(1)

    if xaircontrol_config.verbose:
        print("url %s" % r.url)
        print ("post data %s" % data)
    #collectd.info('%s plugin: Got from API: %s' % (plugin_name, r.text))
    return r.text

''' Copied from https://github.com/python/cpython/blob/2.7/Lib/urlparse.py '''
def parse_qsl(qs, keep_blank_values=0, strict_parsing=0, max_num_fields=None):
    """Parse a query given as a string argument.
    Arguments:
    qs: percent-encoded query string to be parsed
    keep_blank_values: flag indicating whether blank values in
        percent-encoded queries should be treated as blank strings.  A
        true value indicates that blanks should be retained as blank
        strings.  The default false value indicates that blank values
        are to be ignored and treated as if they were  not included.
    strict_parsing: flag indicating what to do with parsing errors. If
        false (the default), errors are silently ignored. If true,
        errors raise a ValueError exception.
    max_num_fields: int. If set, then throws a ValueError if there
        are more than n fields read by parse_qsl().
    Returns a list, as G-d intended.
    """
    # If max_num_fields is defined then check that the number of fields
    # is less than max_num_fields. This prevents a memory exhaustion DOS
    # attack via post bodies with many fields.
    if max_num_fields is not None:
        num_fields = 1 + qs.count('&') + qs.count(';')
        if max_num_fields < num_fields:
            raise ValueError('Max number of fields exceeded')

    pairs = [s2 for s1 in qs.split('&') for s2 in s1.split(';')]
    r = []
    for name_value in pairs:
        if not name_value and not strict_parsing:
            continue
        nv = name_value.split('=', 1)
        if len(nv) != 2:
            if strict_parsing:
                raise ValueError, "bad query field: %r" % (name_value,)
            # Handle case of a control-name with no equal sign
            if keep_blank_values:
                nv.append('')
            else:
                continue
        if len(nv[1]) or keep_blank_values:
            name = unquote(nv[0].replace('+', ' '))
            value = unquote(nv[1].replace('+', ' '))
            r.append((name, value))

    return r

''' Copied from https://github.com/python/cpython/blob/2.7/Lib/urlparse.py '''
def unquote(s):
    """unquote('abc%20def') -> 'abc def'."""
    if _is_unicode(s):
        if '%' not in s:
            return s
        bits = _asciire.split(s)
        res = [bits[0]]
        append = res.append
        for i in range(1, len(bits), 2):
            append(unquote(str(bits[i])).decode('latin1'))
            append(bits[i + 1])
        return ''.join(res)

    bits = s.split('%')
    # fastpath
    if len(bits) == 1:
        return s
    res = [bits[0]]
    append = res.append
    for item in bits[1:]:
        try:
            append(_hextochr[item[:2]])
            append(item[2:])
        except KeyError:
            append('%')
            append(item)
    return ''.join(res)

''' Copied from https://github.com/python/cpython/blob/2.7/Lib/urlparse.py '''
try:
    unicode
except NameError:
    def _is_unicode(x):
        return 0
else:
    def _is_unicode(x):
        return isinstance(x, unicode)

if __name__ == '__main__':  # Independent test, without collectd
    read_func()
else:
    import collectd
    collectd.register_config(config_func)
    collectd.register_init(init)
    collectd.register_read(read_func)
